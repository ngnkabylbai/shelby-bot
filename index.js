process.env["NTBA_FIX_319"] = 1;

const TelegramBot = require('node-telegram-bot-api');
const token = "476358875:AAEbq1JKH95-MlshaEtoH_-HASALnrU2Z48";
const bot = new TelegramBot(token, {polling: true});
const myId = 211140741;
var herId = -1;
// var herId = 528678074;
var isInitialized = false;
// var isInitialized = true;

var waitingForPassword = false;
const password = "justSmokeAndTrouble";

var sleepMode = false;


bot.on('message', function(msg) {
	const chatId = msg.chat.id;
	const message = msg.text;

	bot.sendMessage(myId, chatId + " : " + message);

	if(chatId !== herId && chatId !== myId && isInitialized) {
		bot.sendMessage(chatId, "Я уже нашел свой Грейс, пожажуйста, оставь меня в покое.");
		return;
	}

	if(chatId === myId) {
		const inputArray = message.split(' '); 
		switch(inputArray[0]) {
			case '/ss': // setSleep
				console.log("gif/"+message.slice(7, message.length)+".mp4");
				sleepMode = (inputArray[1] === 'true');
				console.log(sleepMode);
				break;
			case '/th': // toHer
				console.log("Sending her text");
				bot.sendMessage(herId, message.slice(4, message.length));
				break;
			case '/thgif':
				console.log("Sending a gif");
				bot.sendDocument(herId, "gif/"+message.slice(7, message.length)+".mp4");
				break;
			case '/tm': //toMe
				console.log("To Me");
				bot.sendMessage(myId, message.slice(4, message.length));
				break;
			case '/hid': // herId
				console.log("Checking her ID");
				bot.sendMessage(myId, herId);
				break;
			case '/mid': // myId
				console.log("Checking my ID");
				bot.sendMessage(myId, myId);
				break;
			case '/state': // myId
				bot.sendMessage(myId, "My ID : " + myId);
				bot.sendMessage(myId, "Her ID : " + herId);
				bot.sendMessage(myId, "Sleep Mode : " + sleepMode);
				bot.sendMessage(myId, "Initialized : " + isInitialized);
				bot.sendMessage(myId, "Wating for password : " + waitingForPassword);
				break;
		}
	}

	if(msg.text == "/start" && !isInitialized) { onBotStarted(msg); return; } // intro

	if(waitingForPassword && !isInitialized) { authorize(msg); return; } // authorization

})

function onBotStarted(msg) {
	if(msg.chat.id === myId) {
		bot.sendMessage(msg.from.id, "Что же, друг мой, момент истины. Ты знаешь что делать. Удачи.")
		return;
	}

	if(isInitialized) {
		bot.sendMessage(msg.from.id, "Мы уже начали наш путь...")
		return;
	}
	
	bot.sendMessage(msg.from.id, "Рад, что нам по пути. Но дай мне знать, что ты та самая. Ты знаешь пароль? Если да, то напиши.")
	if(!isInitialized)
		waitingForPassword = true;
}

function authorize(msg) {
	if(msg.chat.id === myId) return;

	if(waitingForPassword &&  msg.text == password) {
		herId = msg.from.id;
		bot.sendMessage(herId, "Единственный способ гарантировать мир — внушить, что война безнадежна. Наберись терпения, и удачи тебе. Надеюсь, тебе будет интересно.")
		bot.sendDocument(herId, "gif/drink.mp4");

		bot.sendMessage(myId, "Единственный способ гарантировать мир — внушить, что война безнадежна. Наберись терпения, и удачи тебе. Надеюсь, тебе будет интересно.")
		bot.sendDocument(myId, "gif/drink.mp4");
		
		bot.sendMessage(myId, "Her id: " + herId); // notify myself
		isInitialized = true;
		waitingForPassword = false;
	} else {
		console.log("entered password:" + msg.text)
		bot.sendDocument(msg.from.id, "gif/no.mp4");
		bot.sendDocument(myId, "gif/no.mp4");
	}
}